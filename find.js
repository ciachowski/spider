const shell = require('shelljs');

const output = shell.grep('-l', process.argv[2], 'cache/*').stdout;
console.info(String(output).split(/\n/g).map(str => decodeURIComponent(str.replace('cache/', ''))).join('\n'));
