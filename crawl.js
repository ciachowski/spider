const request = require('request');
const cheerio = require('cheerio');
const crypto = require('crypto');
const url = require('url');
const fs = require('fs');

const visit = (destination, callback) => {
  request(destination, (err, res, html) => {
    if (err || res.statusCode !== 200) {
      return;
    }

    const $ = cheerio.load(html);
    const links = new Set();
    $('a[href]')
      .map((_, element) => String($(element).attr('href') || '').replace(/\/+$/, ''))
      .map((_, href) => {
        if (internalUrl(href)) {
          const parsed = url.parse(destination);
          return parsed.protocol + '\/\/' +  parsed.host + href;
        }
        return href;
      })
      .filter((_, href) => href.length && externalUrl(href))
      .each((_, href) => links.add(href));

    callback(destination, html, Array.from(links));
  });
};
const externalUrl = url => /^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)/g.test(url);
const internalUrl = url => /^\/[-a-zA-Z0-9@:%_\+.~#?&\/=]*/g.test(url);
const sameDomain = (a, b) => getHostname(url.parse(b)) === getHostname(url.parse(a));
const getHostname = url => String(url.hostname || '').replace('www.', '');

const linkSet = new Set();
const visited = new Set();
const crawl = (callback) => {
  const links = Array.from(linkSet).filter(link => !visited.has(link));
  if (links.length === 0) {
    callback();
    return;
  }
  const link = links[0];
  visited.add(link);

  visit(link, (destination, html, links) => {
    links.filter(link => sameDomain(destination, link)).forEach(link => {
      linkSet.add(link);
    });
    console.info('->', destination);
    console.info('<-', crypto.createHash('md5').update(html).digest('hex'));
    console.info('--');
    fs.writeFileSync('cache/' + encodeURIComponent(destination), html);

    crawl(callback);
  });
};

if (!fs.existsSync('cache')){
  fs.mkdirSync('cache');
}
linkSet.add(process.argv[2]);
crawl(() => {
  console.log('Done!');
});

process.on('exit', () => {
  console.log('Exit!');
});
