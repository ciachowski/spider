# Website crawling bot (POC)

Website crawling bot implemented as proof of concept and an example for WSZIB classes.

[Slides for this little project (polish only)](slides.pdf)

## How to install?
> npm install

## API
1. npm start {website_url_to_be_crawled}, e.g. `npm start http://forum.dlang.org`
2. npm run find {keyword}, e.g. `npm find raii` (finds urls which contains given keyword)
3. npm run clear (flushes already crawled pages used for find command)

## How to run?
First thing to do is to crawl given web page and prepare file system based index, for 
example:
> npm start http://forum.dlang.org
```
> node crawl.js "https://forum.dlang.org"
-> https://forum.dlang.org
<- d12f7f227b5353874a7518488fd2c549
--
-> https://forum.dlang.org/group/learn
<- 718c6ac2187f312a75d7eeae6d935b9e
--
-> https://forum.dlang.org/group/general
<- 61658529cd803f44e25705c7bfee2799
--
-> https://forum.dlang.org/group/announce
<- 4403924a85e322a5ac3e357476023293
--

...
```

Having crawled website, we can eventually do something more useful and find some urls which contains given keyword:
> npm run find raii

```
> node find.js "raii"

https://forum.dlang.org/group/announce
https://forum.dlang.org/post/oik0ub$2ln0$1@digitalmars.com
https://forum.dlang.org/thread/bewornepcexaonukvxdn@forum.dlang.org
```

When we're no more interested about created index, we can clear it and start over again:
> npm run clear
